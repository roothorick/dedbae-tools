#pragma once

#include <stdio.h>

void act_recrypt(char* infilename, char* outfilename); // recrypt.c
void act_create(char* nca_filename, char* section_filenames[], unsigned int num_sections); // create.c
// TODO
void act_gencnmt(char* outfilenames, char* nca_filenames[], unsigned int num_files); // gencnmt.c
void act_printcnmt(char* file); // printcnmt.c
void act_xciinfo(char* file); // xciinfo.c
