#include "actions.h"
#include "nca.h"
#include "aes.h"
#include "sha.h"
#include "utils.h"
#include "args.h"

#include <string.h>
#include <byteswap.h>
#include <unistd.h>
#include <sys/sendfile.h>

#define SCRATCHFILENAME ".dedbae.scratch"

void act_recrypt(char* infilename, char* outfilename) {
    nca_header_t old_hdr, new_hdr;
    
    FILE* infile = fopen( infilename, "rb" );
    if(infile == NULL)
        bail("Failed to open input file");
    
    FILE* outfile = fopen( outfilename, "wb" );
    if(outfile == NULL)
        bail("Failed to open output file");
    
    nca_read_header(infile, &old_hdr);
    memcpy( &new_hdr, &old_hdr, sizeof(nca_header_t) );
    
    if(cmdline_args.location != NULL) {
        if( strcmp(cmdline_args.location, "console") == 0 )
            new_hdr.location = LOCATION_CONSOLE;
        else
            new_hdr.location = LOCATION_GAMECARD;
    }
    
    if(cmdline_args.content != NULL) {
        if( strcmp(cmdline_args.content, "program") == 0 )
            new_hdr.content_type = NCA_CONTENT_TYPE_PROGRAM;
        else if( strcmp(cmdline_args.content, "meta") == 0)
            new_hdr.content_type = NCA_CONTENT_TYPE_META;
        else if( strcmp(cmdline_args.content, "control") == 0)
            new_hdr.content_type = NCA_CONTENT_TYPE_CONTROL;
        else if( strcmp(cmdline_args.content, "manual")  == 0)
            new_hdr.content_type = NCA_CONTENT_TYPE_MANUAL;
        else
            new_hdr.content_type = NCA_CONTENT_TYPE_DATA;
    }
    
    // Use TitleID + 00 padding as RightsID, it seems to work for eshop rips
    
    // TitleID is stored LSB, RightsID is stored MSB
    //char* titleid_bytes = (char*) &old_hdr.titleid;
    //for(int i=0; i<8; i++) {
    //    int j=7-i;
    //    new_hdr.rights_id[i] = titleid_bytes[j];
    //}
    
    for(int i=0; i<4; i++) {
        if( old_hdr.section_table[i].media_offset == 0x0 )
            // No more sections
            break;
            
        else if( old_hdr.section_headers[i].cryptotype == SECTION_CRYPTO_TYPE_PLAINTEXT ) {
            // Copy directly into the output without changes
            nca_seek_to_section(&new_hdr, outfile, i);
            nca_raw_copy_section(&old_hdr, infile, outfile, i);
            
            continue;
        }
        
        // Decrypt to a scratch file
        FILE* scratch = fopen(SCRATCHFILENAME, "wb+");
        nca_extract_section(&old_hdr, infile, scratch, i);
        
        // Encrypt to new file
        fseek(scratch, 0, SEEK_SET);
        nca_replace_section(&new_hdr, scratch, outfile, i);
        fclose(scratch);
        unlink(SCRATCHFILENAME);
    }
    
    nca_write_header(&new_hdr, outfile);
    
    fclose(outfile);
    fclose(infile);
}
