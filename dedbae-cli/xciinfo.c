#include "actions.h"

#include "xci.h"
#include "hfs0.h"

#include <stdio.h>
#include <stdlib.h>

void ls_hfs0(FILE* fd) {

	hfs0_header_t header;
	fread(&header, 1, sizeof(hfs0_header_t), fd);
	// File position is now start of file entry list
	
	hfs0_file_entry_t* entries = malloc( sizeof(hfs0_file_entry_t) * header.num_files);
	fread(entries, 1, sizeof(hfs0_file_entry_t) * header.num_files, fd);
	// File position is now start of stringtable
	
	char* stringtable = malloc(header.stringtable_size);
	fread(stringtable, 1, header.stringtable_size, fd);
	// File position is now start of file data area

	printf("Number of files : %u\n", header.num_files);
	
	for(int i=0; i<header.num_files; i++) {
		printf("    %s:\n", &stringtable[ entries[i].file_name_offset ] );
		
		printf("            File offset: %lx\n", entries[i].file_offset);
		printf("            Size       : %lu bytes\n", entries[i].file_size);
		printf("\n");
	}
	
	free(stringtable);
	free(entries);
}

void act_xciinfo(char* file) {
	xci_header_t header;
	
	FILE* fd = fopen(file, "rb");
	
	printf("XCI Header:\n");
	fread(&header, 1, sizeof(xci_header_t), fd);
	
	printf("Secure area starts at: 0x%llx\n", header.secure_area_start * 0x200ULL);
	printf("Titlekek index       : %u\n", header.titlekek_index);
	printf("Gamecard size        : %u GB\n", xci_gamecard_size_to_int(header.cardsize));
	printf("XCI header version   : %u\n", header.header_version);
	
	printf("Autoboot             : ");
	if(header.flags & GAMECARD_FLAG_AUTOBOOT)
		printf("yes");
	else
		printf("no");
	printf("\n");
	
	printf("History Erase        : ");
	if(header.flags & GAMECARD_FLAG_HISTORY_ERASE)
		printf("yes");
	else
		printf("no");
	printf("\n");
	
	printf("Package ID           : 0x%016lx\n", header.packageid);
	printf("Valid data ends at   : 0x%llx\n", header.valid_data_end * 0x200ULL);
	printf("HFS0 offset          : 0x%lx\n", header.hfs0_offset);
	printf("HFS0 header size     : 0x%lx\n", header.hfs0_headersize);
	printf("\n");

	// TODO: Gamecard info, and *maybe* certificate?
	
	printf("Master HFS0:\n");
	
	fseek(fd, header.hfs0_offset, SEEK_SET);
	ls_hfs0(fd);
	
	fseek(fd, header.hfs0_offset, SEEK_SET);
	hfs0_header_t masterhfs_header;
	fread(&masterhfs_header, 1, sizeof(hfs0_header_t), fd);
	// File position at start of file entry table
	
	uint64_t masterhfs_stringtable_start = ftell(fd) + 
		masterhfs_header.num_files * sizeof(hfs0_file_entry_t);
	
	char* stringtable = malloc(masterhfs_header.stringtable_size);
	fseek(fd, masterhfs_stringtable_start, SEEK_SET);
	fread(stringtable, 1, masterhfs_header.stringtable_size, fd);
	
	uint64_t masterhfs_data_start = masterhfs_stringtable_start + 
			masterhfs_header.stringtable_size;
	
	fseek(fd, header.hfs0_offset + 0x10, SEEK_SET); // Start of file entry table
	
	for(int i=0; i<masterhfs_header.num_files; i++) {
		hfs0_file_entry_t entry;
		fread(&entry, 1, sizeof(hfs0_file_entry_t), fd);
		
		 printf( "%s: ", &stringtable[ entry.file_name_offset ] );
		
		uint64_t filepos = ftell(fd);
		fseek(fd, masterhfs_data_start + entry.file_offset, SEEK_SET);
		ls_hfs0(fd);
		fseek(fd, filepos, SEEK_SET);
	}
	
	free(stringtable);
	
	fclose(fd);
}
