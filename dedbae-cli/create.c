#include "actions.h"

#include "nca.h"
#include "pfs0.h"
#include "sha.h"
#include "utils.h"

#include <string.h>
#include <math.h>

void act_create(char* nca_filename, char* section_filenames[], unsigned int num_sections) {
	nca_header_t header;
	
	nca_init_header(&header, NULL);
	
	FILE* nca = fopen(nca_filename, "wb");
	
	for(int i=0; i<num_sections; i++) {
		FILE* section = fopen(section_filenames[i], "rb");
		
		nca_section_header_t* sec_hd = &header.section_headers[i];
		pfs0_header_t pfs_hd;
		
		fread(&pfs_hd, 1, sizeof(pfs0_header_t), section);
		if( strncmp(pfs_hd.magic, "PFS0", 4) != 0 )
			bail("Unrecognized file format. (supports PFS0 only for now)");
		
		if(i==0)
			header.section_table[i].media_offset = 6; // End of NCA header
		else
			header.section_table[i].media_offset = header.section_table[i-1].media_end;
		
		nca_inject_pfs0(&header, section, nca, i);
		
		fclose(section);
	}
	
	fseek(nca, 0, SEEK_END);
	header.filesize = ftell(nca);
	nca_write_header(&header, nca);
	fclose(nca);
}
