#include "actions.h"

#include "cnmt.h"
#include "types.h"
#include "nca.h"
#include "utils.h"
#include "args.h"

#include <stdlib.h>

void act_gencnmt(char* outfilename, char* nca_filenames[], unsigned int num_files) {
	cnmt_header_t cnmt_header;
	cnmt_content_record_t* content_records;
	char digest[32];
	
	memset(&cnmt_header, 0, sizeof(cnmt_header_t));
	if( cmdline_args.digest != NULL )
		memcpy(digest, cmdline_args.digest, 32);
	else if( cmdline_args.oldfile != NULL ) {
		FILE* oldfile = fopen(cmdline_args.oldfile, "rb");
		fseek(oldfile, -32, SEEK_END);
		fread(digest, 1, 32, oldfile);
		fclose(oldfile);
	}
	else
		memset(digest, 0, 32);
	
	content_records = malloc(sizeof(cnmt_content_record_t) * num_files);
	memset(content_records, 0, sizeof(cnmt_content_record_t) * num_files);
	
	cnmt_init_header(&cnmt_header, NULL);
	
	for(int i=0; i<num_files; i++) {
		FILE* nca = fopen(nca_filenames[i], "rb");
		if(nca == NULL)
			bail("Failed to open input file");
		
		// Extract title ID from the first NCA
		if(i==0) {
			nca_header_t hdr;
			nca_read_header(nca, &hdr);
			cnmt_header.titleid = hdr.titleid;
		}
		
		printf("Generating content record for %s...", nca_filenames[i]);
	 	cnmt_create_content_record(nca, &content_records[i]);
		printf("Done!\n");
		
		fclose(nca);
	}
	
	cnmt_header.content_count = num_files;
	
	printf("Writing CNMT...");
	FILE* outfile = fopen(outfilename, "wb+");
	if(outfile == NULL)
		bail("Failed to open output file");
	fwrite(&cnmt_header, 1, sizeof(cnmt_header_t), outfile);

	
	for(int i=0; i<num_files; i++)
		fwrite(&content_records[i], 1, sizeof(cnmt_content_record_t), outfile);
	
	fwrite(digest, 1, 32, outfile);
	
	fclose(outfile);
	printf("Done!\n");
	
	free(content_records);
}
